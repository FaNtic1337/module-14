#include <iostream>
#include <string>

int main()
{
	std::cout << "Enter something:\n";
	std::string any_string;
	std::getline(std::cin, any_string);

	std::cout << "You wrote: " << any_string;
	std::cout << "\nThe length of this string: " << any_string.length();
	std::cout << "\nThe first symbol is '" << any_string[0] << "'";
	std::cout << "\nThe last symbol is '" << any_string[any_string.length() - 1] << "'";

	return 0;
}